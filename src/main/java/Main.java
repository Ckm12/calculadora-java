public class Main {

    public static void main(String args[]) {

        // Casio calculator
        CalculadoraCasio calculadoraCasio = new CalculadoraCasio();
        calculadoraCasio.marca = "Cassio";
        calculadoraCasio.serial = "45881";

        Integer resultadoSuma = calculadoraCasio.sumar(1, 2);
        System.out.println("El resultado de la summa es es " + resultadoSuma);

        Integer resultadoResta = calculadoraCasio.restar(5, 7);
        System.out.println(("El resulltado de la resta es " + resultadoResta));

        Double resultadoMultipli = calculadoraCasio.multipli(5, 7);
        System.out.println(("El resulltado de la multiplicaion es " + resultadoMultipli));

        Double resultadoDiv = calculadoraCasio.div(5, 7);
        System.out.println(("El resulltado de la division es " + resultadoDiv));

        Double resultadoPot = calculadoraCasio.potencia(5,8);
        System.out.println(("El resultado de la potencia es: "+resultadoPot));


        // China calculator

        CalculadoraChina calculadoraChina = new CalculadoraChina();
        calculadoraChina.serial = "666";

        Integer sumaResultadoC = calculadoraChina.sumar(1,2);
        System.out.println("La suma da como resultado: " + sumaResultadoC);

        Integer restaResultadoC = calculadoraChina.restar(10,5);
        System.out.println("La resta da como resultado: " + restaResultadoC);

        Double divResultadoC = calculadoraChina.div(10,2);
        System.out.println("La división da como resultado: " + divResultadoC);

        Double multipliResultadoC= calculadoraChina.multipli(5,5);
        System.out.println("La multiplicación da como resultado: " + multipliResultadoC);

        Double resultadoPotC = calculadoraCasio.potencia(5,8);
        System.out.println(("El resultado de la potencia es: "+resultadoPotC));


        //Calculadora Alemana

        System.out.println(" - CALCULADORA ALEMANA - ");

        CalculadoraAlemana calculadoraAlemana = new CalculadoraAlemana(1000000);
        calculadoraAlemana.serial = "1512";

        Integer resultadoSumaAlemana = calculadoraAlemana.sumar(1, 2);
        System.out.println("La suma da como resultado: " + resultadoSumaAlemana);

        Integer resultadoRestaAlemana = calculadoraAlemana.restar(10, 5);
        System.out.println("La resta da como resultado: " + resultadoRestaAlemana);

        Integer resultadoDivisionAlemana = calculadoraAlemana.dividir(10, 2);
        System.out.println("La división da como resultado: " + resultadoDivisionAlemana);

        Integer resultadoMultiplicarAlemana = calculadoraAlemana.multiplicar(5, 5);
        System.out.println("La multiplicación da como resultado: " + resultadoMultiplicarAlemana);

        System.out.println("La vida util de la calculadora es " + calculadoraAlemana.getVidaUtil());

        System.out.println(" - CALCULADORA COLOMBIANA - ");


    }}