
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TesterCalculadora {
    @Test
    public  void testSuma(){
        CalculadoraCasio calculadora = new CalculadoraCasio();

        Integer resultadoSuma= calculadora.sumar(2,5);
        Assertions.assertEquals(7,resultadoSuma);
    }

    @Test
    public void testResta (){
        CalculadoraCasio calculadora = new CalculadoraCasio();
        Integer resultadoResta = calculadora.restar(6,2);
        Assertions.assertEquals(4,resultadoResta);
    }
    @Test
    public void testMultipli (){
        CalculadoraCasio calculadora = new CalculadoraCasio();
        Double resultadoMultipli = calculadora.multipli(4,2);
        Assertions.assertEquals(8,resultadoMultipli);
    }

    @Test
    public void testDiv() {
        CalculadoraCasio calculadora = new CalculadoraCasio();
        Double resultadoDiv = calculadora.div(10, 2);
        Assertions.assertEquals(5, resultadoDiv);
    }

    @Test
    public void testPotencia() {
        CalculadoraCasio calculadora1 = new CalculadoraCasio();
        Double resultadoPot = calculadora1.potencia(5, 2);
        Assertions.assertEquals(25, resultadoPot);


        }

    }